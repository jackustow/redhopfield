import numpy as np


def imprimir_imagen(matriz: np.ndarray) -> np.ndarray:

    # Reemplaza los -1 por un espacio y los 1 por una equis
    print(np.where(matriz == -1, " ", "x"), "\n")
