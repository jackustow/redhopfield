import numpy as np
from os import system, name
import entrenarRed as entRed
import reconocerImagen as recImg

# Creamos una matriz de entrenamiento
matrizent = np.zeros((100, 100))

# Creamos una matriz para hallar
matrizhall = np.array([[-1, -1,  1,  1, -1, -1,  1,  1, -1, -1],
                       [-1, -1,  1,  1,  1,  1,  1,  1, -1, -1],
                       [-1,  1, -1, -1, -1, -1, -1, -1, -1, -1],
                       [-1,  1, -1, -1, -1, -1, -1, -1, -1, -1],
                       [-1,  1, -1, -1, -1, -1, -1, -1, -1, -1],
                       [ 1,  1, -1, -1, -1, -1, -1, -1, -1, -1],
                       [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                       [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                       [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                       [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
                       ], dtype=int)


# Funcion de limpieza de la consola
def clear():
    # for windows
    if name == 'nt':
        _ = system('cls')
        # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')


if __name__ == "__main__":

    while 1 == 1:
        # Menú de la aplicación
        print("+--------------------------------+")
        print("|        REDES DE HOPFIELD       |")
        print("|                                |")
        print("|       Jaime Andrés Cruz Romero |")
        print("|                    UNAULA - IA |")
        print("+--------------------------------+")
        print("1 - Entrenar la red")
        print("2 - Encontrar caracter en la red")
        print("99 - Salir")

        #try:
        mnu = int(input('Seleccione una opción:'))

        print("Ha seleccionado la opción", mnu)

        # Opción de entrenamiento
        if mnu == 1:

            while 1 == 1:
                # Seleccion de caracter a entrenar
                print("+--------------------------------+")
                print("Seleccione el caracter a entrenar:")
                print("   1 - Linea horizontal superior")
                print("   2 - Linea horizontal inferior")
                print("   3 - Cara feliz")
                print("   4 - Signo admiración")
                print("   5 - Signo interrogación")
                print("   6 - Equiz hacia derecha")
                print("   7 - Robot")
                print("   8 - Signo adición pixel")
                print("   9 - Cruz esvástica")
                print("  10 - Equiz hacia izquierda")
                print("  99 - Salir")

                #try:
                opc = int(input('Seleccione una opción:'))

                print("Ha seleccionado la opción", opc)

                if opc <= 10:
                    matriz = entRed.entrenar_imagen(opc)
                    matrizent = matrizent + matriz

                elif opc == 99:
                    print("Regresa a menú principal")
                    clear()
                    break

                #except ValueError:
                    #print("Opcion errada. Inténtelo de nuevo")
                    #sleep(1)

        # Finaliza la aplicación
        elif mnu == 2:
            recImg.reconoce_imagen(matrizent, matrizhall)

        elif mnu == 99:
            print("Hasta luego!")
            break

        #except ValueError:
            #print("Opcion errada del menú. Inténtelo de nuevo")
            #sleep(2)
            #clear()
