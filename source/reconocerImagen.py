import numpy as np
import math
import imprimeImagen as impImg

porc_exactitud = 50
elementos_eval = 0
elementos_minimos = 0

# Funcion para calculas la multiplicacion matricial entre dos arrays
# (https://claudiovz.github.io/scipy-lecture-notes-ES/intro/numpy/operations.html)
# y devuelve el valor unitario
def multiplica(matriz01: np.ndarray, matriz02: np.ndarray) -> np.ndarray:

    # Realiza la multiplicacion matricial
    matrizmult = matriz01.dot(matriz02)

    # Calcula el valor unitario
    matriz = matrizmult / (abs(matrizmult))

    return matriz

def reconoce_imagen(matrizent, matrizhall):
    print("Matriz a reconocer")
    impImg.imprimir_imagen(matrizhall)

    print("Porcentaje mínimo de coincidencia:", porc_exactitud)

    # Halla la dimensión de la matriz a hallar
    tamano = matrizhall.size
    dimension = tamano / math.sqrt(tamano)

    # Calcula la cantidad de elementos coincidentes minimas según el porcentaje de exactitud
    elementos_minimos = int((tamano * porc_exactitud) / 100)

    # Convertimos la matriz a hallar en una matriz fila
    matrizini = matrizhall.reshape(matrizhall.size)

    # Multiplicamos la matriz fila con la matriz entrenada (multiplicacion matricial)
    matriz01 = multiplica(matrizini, matrizent)

    while 1 == 1:

        # Multiplicamos a matriz01 con la matriz entrenada (multiplicación matricial) para comparación
        matriz02 = multiplica(matriz01, matrizent)

        #print("matriz01")
        #print(matriz01.astype(int))
        #print("matriz02")
        #print(matriz02.astype(int))
        #print("\n")

        # Evalúa si el porcentaje de coincidencia es el establecido
        matrizeval = np.isclose(matriz01, matriz02)
        elementos_eval = np.sum(matrizeval)

        #print("eval", matrizeval)
        #print("total eval", elementos_eval)

        #print("Coinciden", elementos_eval, "de", tamano, "elementos")

        if elementos_eval >= elementos_minimos:
            # Convierte resultado a matriz nxn (para impresion de evolucion de la red)
            a = matriz02.reshape(int(dimension), int(dimension))
            impImg.imprimir_imagen(a)

            break
        else:
            matriz01 = matriz02
